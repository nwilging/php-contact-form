New Contact Request:

Name: {{ $contact->full_name }}
Phone: @if($contact->phone) {{ $contact->phone }} @else N/A @endif

Email: {{ $contact->email }}
Message:

{{ $contact->message }}