<!-- Contact Section -->
<section id="contact" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2>Contact Guy Smiley</h2>
            <p>Remember Guy Smiley?  Yeah, he wants to hear from you.</p>
            @if(\Illuminate\Support\Facades\Session::has('contacted'))
                <div class="alert alert-block alert-success">Thank you for your message!</div>
            @else
                <form action="{{ url('/contact') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="full_name" value="{{ old('full_name') }}" placeholder="Full Name *" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="phone_number" value="{{ old('phone_number') }}" placeholder="Phone Number" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Email Address *" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" placeholder="Write your message..." style="height:150px; resize:none" required>{{ old('message') }}</textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default pull-right" type="submit">Send Message</button>
                    </div>
                </form>
                <div class="clearfix"></div>
                <br>
                @if($errors->any())
                    <div class="alert alert-block alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            @endif
        </div>
    </div>
</section>