<?php
/*
 * Nick Wilging
 * 12/19/17
 *
 * Dealer Inspire
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    protected $fillable = [
        'full_name',
        'phone_number',
        'email',
        'message'
    ];

    public $table = 'contact_requests';

    public $primaryKey = 'id';
}