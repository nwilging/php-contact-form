<?php
/*
 * Nick Wilging
 * 12/19/17
 *
 * Dealer Inspire
 */

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller {

    public function index() {
        return view('home');
    }

    public function contact(Request $request) {
        # Had to write a custom validator to redirect back with the anchor tag "#contact"
        $validator = Validator::make($request->all(), [
            'full_name'     => 'required|max:255',
            'email'         => 'required|max:255',
            'message'       => 'required|max:10000'
        ]);
        if($validator->fails()) {
            return redirect('/#contact')->withErrors($validator)->withInput();
        }

        # Validator passed!
        $contact = Contact::create($request->all());
        Mail::send('emails.contact', ['contact' => $contact], function($m) use ($contact) {
            $m->from('hello@dealerinspire.com', 'Dealer Inspire');
            $m->to('guy-smiley@example.com', 'Guy Smiley')->subject('Contact Request');
        });
        Session::put('contacted', true);
        return redirect('/#contact');
    }
}