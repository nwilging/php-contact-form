<?php

namespace Tests\Feature;

use App\Models\Contact;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactTest extends TestCase
{

    public function testContactForm() {
        $contact = new Contact([
            'full_name'     => 'Test Name',
            'email'         => 'test@test.com',
            'phone_number'  => '123 456 7890',
            'message'       => 'TEST MESSAGE'
        ]);

        $this->assertEquals('Test Name', $contact->full_name);
        $this->assertEquals('test@test.com', $contact->email);
        $this->assertEquals('123 456 7890', $contact->phone_number);
        $this->assertEquals('TEST MESSAGE', $contact->message);

        $contact->save();

        # Test retrieving from db
        $pulled = Contact::find($contact->id);

        $this->assertEquals('Test Name', $pulled->full_name);
        $this->assertEquals('test@test.com', $pulled->email);
        $this->assertEquals('123 456 7890', $pulled->phone_number);
        $this->assertEquals('TEST MESSAGE', $pulled->message);

        $pulled->delete();
    }
}
